# Warningsystem

Warning system based on ARINC 653 and implementation on stm32 nucleo


ARINC 
Category	                    Header file
Partition management	        apex_partition.h
Process Management	            apex_process.h
Time management             	apex_time.h
Memory management	            No APEX calls
Interpartition communication	apex_sampling.h apex_queuing.h
Intrapartition communication	apex_buffer.h apex_blackboard.h
                                apex_semaphore.h
                                apex_event.h"
Health monitoring	            apex_error.h
